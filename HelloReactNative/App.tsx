/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
//import type {PropsWithChildren} from 'react';
import {
  //SafeAreaView,
  //ScrollView,
  StatusBar,
  //StyleSheet,
  // Text,
  useColorScheme,
  //View,
} from 'react-native';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {
  Colors,
  // DebugInstructions,
  // Header,
  //LearnMoreLinks,
  // ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Home from './Home';
import AddTaskForm from './AddTaskForm';
import ListTask from './ListTask';

const Stack = createStackNavigator();

/*
type SectionProps = PropsWithChildren<{
  title: string;
}>;

/*
function Section({children, title}: SectionProps): JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
}
*/

function App(): JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen
            name="Home"
            options={{
              headerShown: false,
              headerMode: 'screen',
            }}>
            {props => <Home {...props} greeting="Helloworld" />}
          </Stack.Screen>
          <Stack.Screen
            name="AddTaskForm"
            component={AddTaskForm}
            options={{
              headerShown: false,
              headerMode: 'screen',
            }}
          />
          <Stack.Screen
            name="ListTask"
            component={ListTask}
            options={{
              headerShown: false,
              headerMode: 'screen',
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

/*
const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});*/

export default App;
