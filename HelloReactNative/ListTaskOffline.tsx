/**
 * Listing Tasks
 * @author Arun Gopi
 */

import React from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import Tasks from './TasksDb';
import TaskItem from './TaskItem';

const TaskListingOffline = ({route}) => {
  const task = route.params;
  Tasks.push(task);

  return (
    <View style={styles.container}>
      <FlatList
        data={Tasks}
        renderItem={({item}) => <TaskItem item={item} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 35,
    backgroundColor: '#fff',
    margin: 10,
    padding: 5,
  },
});

export default TaskListingOffline;
