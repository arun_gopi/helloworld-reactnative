/**
 * Listing Tasks by getting it from remote server
 * @author Arun Gopi
 */

import React, {useEffect, useState} from 'react';
import {View, ActivityIndicator, StyleSheet, FlatList} from 'react-native';
import Tasks from './TasksDb';
import TaskItem from './TaskItem';

const TaskListing = ({route}) => {
  const task = route.params;
  
  const [isLoading, setDataLoaded] = useState(true);
  const [remoteTasks, setRemoteTasks] = useState<Object>();

  const getTasks = async () => {
    try {
      //TODO Get tasks from remote server.Offline data, to be replace with data fetched from remote server
      setRemoteTasks(Tasks);
      setDataLoaded(false);
    } catch (error) {
      console.log(error);
    }
  };

  // This hook runs when this component is loaded
  useEffect(() => {
    Tasks.push(task);
    getTasks();
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={remoteTasks}
          renderItem={({item}) => <TaskItem item={item} />}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 35,
    backgroundColor: '#fff',
    margin: 10,
    padding: 5,
  },
});

export default TaskListing;
