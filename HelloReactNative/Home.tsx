/**
 * Sample Home componet
 * @author Arun Gopi
 */

import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import Menu from './Menu';


const Home = props => {
  return (
    <View>
      <Text style={styles.header}>Task Manager</Text>
      <View style={styles.menu}>
        <Menu />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    fontSize: 30,
    fontWeight: 'bold',
    margin: 20,
  },
  menu: {
    position: 'absolute',
    top: 60,
    color: 'green',
    // how to add color?
  },
});

export default Home;
