/**
 * Lists tasks
 * @author Arun Gopi
 */

import React from 'react';
import {View, Text, FlatList} from 'react-native';

const Tasks = () => {
  const taskItem = props => {
    return (
      <View>
        <Text>{props.task.name}</Text>
      </View>
    );
  };

  return (
    <FlatList data={[]} renderItem={taskItem} keyExtractor={item => item.id} />
  );
};

export default Tasks;
