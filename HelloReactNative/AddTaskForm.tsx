/**
 * Form to add Task
 * @author Arun Gopi
 */

import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Alert, Button} from 'react-native';

const AddTaskForm = ({navigation}) => {
  const [taskName, setTaskName] = useState('Enter Task name');
  const [taskDescription, setTaskDescription] = useState(
    'Enter Task description',
  );

  const saveInCloud = async (data: any) => {
    console.log(data);
    //TODO Call the API here to save the task
    //TODO Get the remote save status to update the current UI and navigate to List task screen
  };

  const submit = () => {
    if (!taskName || !taskDescription) {
      Alert.alert('Please fill all fields');
    } else {
      try {
        saveInCloud({
          id: Math.random(),
          name: taskName,
          description: taskDescription,
        });
      } catch (error) {
        console.log(error);
      }

      //Alert.alert('Task entered');
      navigation.navigate('ListTask', {
        id: Math.random(),
        name: taskName,
        description: taskDescription,
      });
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Name *required : </Text>
      <TextInput
        style={styles.textInput}
        value={taskName}
        onChangeText={name => setTaskName(name)}
        selectTextOnFocus={true}
      />
      <Text style={styles.title}>Description *required : </Text>
      <TextInput
        style={styles.textInput}
        value={taskDescription}
        onChangeText={name => setTaskDescription(name)}
        selectTextOnFocus={true}
      />
      <Button title="Add" color="#708090" onPress={submit} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 35,
    backgroundColor: '#fff',
    margin: 10,
    padding: 5,
  },
  header: {
    fontSize: 30,
    fontWeight: 'bold',
    margin: 20,
  },
  title: {
    textAlign: 'center',
    color: '#87CEEB',
  },
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    backgroundColor: 'white',
    alignSelf: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 10,
    margin: 5,
  },
});

export default AddTaskForm;
