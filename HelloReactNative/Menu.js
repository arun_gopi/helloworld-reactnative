import React from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const Menu = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.menu}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('AddTaskForm');
        }}
        style={styles.button}>
        <Text style={styles.buttonText}>Add Task</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  menu: {
    flexDirection: 'row',
    margin: 20,
  },
  button: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    marginLeft: 10,
    marginRight: 10,
  },

  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'green',
  },
});

export default Menu;
