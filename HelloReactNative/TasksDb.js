/**
 * Temporary sample database to store tasks
 * @author Arun Gopi
 */
var Tasks = [
  {
    id: 1,
    name: 'Purchase',
    description: 'Purchase stationary',
  },
  {
    id: 2,
    name: 'Cooking',
    description: 'Make dinner for the family',
  },
  {
    id: 3,
    name: 'Cleaning',
    description: 'Clean the house',
  },
  {
    id: 4,
    name: 'Gym',
    description: 'Go to gym',
  },
  {
    id: 5,
    name: 'Reading',
    description: 'Read a book',
  },
];

export default Tasks;
