import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const TaskItem = ({item}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.customText}>{item.name}</Text>
      <Text style={styles.customText}>{item.description}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {margin: 10, padding: 10, backgroundColor: '#eee'},
  customText: {
    fontSize: 20,
  },
});

export default TaskItem;
